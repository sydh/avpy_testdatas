===============
Avpy test datas
===============

Avpy_testDatas is used by Avpy (see https://bitbucket.org/sydh/avpy) to provide test medias for automated tests.

Install
=======

Requirements
------------

Setup
-----

wav:

python src/genWav.py

tiff:

python src/genTiff.py # require PIL or Pillow

video:

wget http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4

Contact
=======

sydhds __at__ gmail __dot__ com


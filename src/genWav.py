import struct
import wave

if __name__ == '__main__':

    wavOut = wave.open('constant.wav', 'w')
    wavOut.setparams(
            (2, 2, 44100, 0, 'NONE', 'not compressed')
            )

    values = []
    wavLen = 88200 # 2s 

    for i in range(0, wavLen):
        
        #if i == wavLen-1:
            #valueL = 2
            #valueR = 1
        #else:
            #valueL = 1
            #valueR = 2
        valueL = 1
        valueR = 2
        valueLPacked = struct.pack('h', valueL)
        valueRPacked = struct.pack('h', valueR)
        # stereo
        values.append(valueLPacked)
        values.append(valueRPacked)


    valuesStr = ''.join(values)
    wavOut.writeframes(valuesStr)
    wavOut.close()

